define({ "api": [
  {
    "type": "get",
    "url": "/agent",
    "title": "Get agent infos",
    "name": "GetAgent",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.infos",
            "description": "<p>Agent infos.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.infos.login",
            "description": "<p>Login.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.infos.email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.infos.registration_date",
            "description": "<p>Registeration date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "data.infos.double_auth",
            "description": "<p>2-factor authentication method.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"login\": \"mikayla\",\n        \"email\": \"mikayla@mikayla.ee\",\n        \"registration_date\": 1517233312,\n        \"double_auth\": 0\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent",
    "title": "Create a new account",
    "name": "PostAgent",
    "group": "Agent",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password already hashed with mui_hash.js. You have to check length and if password confirm match in client side.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Email address.</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": true,
            "field": "doubleAuth",
            "description": "<p>If 1, enable 2-factor authentication by mail, if 2, enable 2-factor authentication by Google Authenticator.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Created 201": [
          {
            "group": "Created 201",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;created&quot;.</p>"
          },
          {
            "group": "Created 201",
            "type": "Int",
            "optional": false,
            "field": "data",
            "description": "<p>Agent ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - loginExists": [
          {
            "group": "Error 400 - loginExists",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;loginExists&quot;. Login already exists.</p>"
          }
        ],
        "Error 400 - mailExists": [
          {
            "group": "Error 400 - mailExists",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;mailExists&quot;. Email already exists.</p>"
          }
        ],
        "Error 400 - loginFormat": [
          {
            "group": "Error 400 - loginFormat",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;loginFormat&quot;. Username format is incorrect (between 2 and 20 alphanumeric characters, -_. allowed).</p>"
          }
        ],
        "Error 400 - mailFormat": [
          {
            "group": "Error 400 - mailFormat",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;mailFormat&quot;. Email format is incorrect.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent/changeAuth",
    "title": "Change 2-factor authentication method",
    "name": "PostChangeAuth",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "doubleAuth",
            "description": "<p>If 1, enable 2-factor authentication by mail, if 2, enable 2-factor authentication by Google Authenticator.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "code",
            "description": "<p>Code generated by Google Authenticator needed for enabling or disabling Google Authenticator.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Updated.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 403 - badCode": [
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badCode&quot;.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent/changeLang",
    "title": "Update lang in database according to language header",
    "name": "PostChangeLang",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent/changeLogin",
    "title": "Change username",
    "name": "PostChangeLogin",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>Username.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - loginExists": [
          {
            "group": "Error 400 - loginExists",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 400 - loginExists",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;loginExists&quot;. Login already exists.</p>"
          }
        ],
        "Error 400 - loginFormat": [
          {
            "group": "Error 400 - loginFormat",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 400 - loginFormat",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;loginFormat&quot;. Username format is incorrect (between 2 and 20 alphanumeric characters, -_. allowed).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent/changeMail",
    "title": "Change email",
    "name": "PostChangeMail",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Email address.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Email has been updated.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - mailExists": [
          {
            "group": "Error 400 - mailExists",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 400 - mailExists",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;mailExists&quot;. Email already exists.</p>"
          }
        ],
        "Error 400 - mailFormat": [
          {
            "group": "Error 400 - mailFormat",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 400 - mailFormat",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;mailFormat&quot;. Email format is incorrect.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/agent/changePassword",
    "title": "Change password",
    "name": "PostChangePassword",
    "group": "Agent",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_pwd",
            "description": "<p>Old password already hashed.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_pwd",
            "description": "<p>New password url encoded. You have to check length and if password confirm match in client side.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;removeToken&quot;. Token has to be removed from client.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - badOldPass": [
          {
            "group": "Error 400 - badOldPass",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 400 - badOldPass",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badOldPass&quot;. Old password does not match.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/agent.coffee",
    "groupTitle": "Agent"
  },
  {
    "type": "post",
    "url": "/answer",
    "title": "Add an answer",
    "name": "AddAnswer",
    "group": "Answer",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "secret_key",
            "description": "<p>Secret Key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Contains answer_id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"answer_id\": 1,\n        \"email\": \"mikayla@mikayla.ee\",\n        \"registration_date\": 1517233312,\n        \"double_auth\": 0\n    },\n    \"message\": \"sent\",\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;notAuthorized&quot;. Not logged (agent side) or bad secret key.</p>"
          }
        ],
        "Error 401 - requestClosed": [
          {
            "group": "Error 401 - requestClosed",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;requestClosed&quot;. Request is closed.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/answer.coffee",
    "groupTitle": "Answer"
  },
  {
    "type": "get",
    "url": "/GoogleAuthenticator/backupCodes",
    "title": "Get backup codes",
    "name": "GetGoogleAuthenticatorBackupCodes",
    "group": "GoogleAuthenticator",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Code generated by Google Authenticator to access backup codes.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.backupCodes",
            "description": "<p>Backup codes.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"backupCodes\": [\n            \"8LgtxIgOvt\",\n            \"LrLmeryfCq\",\n            \"ytqt1RBbEM\",\n            \"CEF4rsN4yI\",\n            \"fBU0K3Zrwe\",\n            \"KAgvEydHGW\",\n            \"4ajP5KvOWM\",\n            \"IZS8WXC7CO\",\n            \"mBJ8QUK1gU\",\n            \"E4nUSomNqx\"\n        ]\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 403 - badCode": [
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badCode&quot;.</p>"
          }
        ],
        "Error 401 - notDoubleAuthGA": [
          {
            "group": "Error 401 - notDoubleAuthGA",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 401 - notDoubleAuthGA",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;notDoubleAuthGA&quot;.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/googleauthenticator.coffee",
    "groupTitle": "GoogleAuthenticator"
  },
  {
    "type": "post",
    "url": "/GoogleAuthenticator/generate",
    "title": "Get QR code, secrey key, and backup codes",
    "name": "PostGoogleAuthenticatorGenerate",
    "group": "GoogleAuthenticator",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.QRcode",
            "description": "<p>QR code image in base 64.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.secretKey",
            "description": "<p>Secret key to register key in Google Authenticator.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.backupCodes",
            "description": "<p>10 backup codes.</p>"
          }
        ],
        "Success 200 - wait": [
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;wait&quot;. Wait 30s before generating a new QR code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"QRcode\": \"iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAABmJLR0QA/wD/AP+gvaeTAAAFbUlEQVR4nO3dwY6lNhRF0a4o///LnUEmSC1H5tnb8KK1hiUwVPeRyzKXy8/v379/wW5/PX0D/D8JFgnBIiFYJASLhGCRECwSgkVCsEgIFgnBIiFYJASLhGCRECwSgkVCsEj8/dlpPz8/e+/j169fo1rW0bVmal+v516Pnxnz7rm7xrl77t17u+uzGmMzFgnBIiFYJD5cY12tvOdzd02w652iu2udXfdTrIFGTv6//MmMRUKwSAgWiQ1rrKu7ezx3x7m7rzNz7sx17455d49qZNce1a7/l3lmLBKCRUKwSGxeY9VG64CV9cHKmq9+/jizLnwnMxYJwSIhWCS+YI1VP9e7O+bd/bNdY6787ueZsUgIFgnBIrF5jVX8vV95xjdzTFF3NeNkLdr5dZgZi4RgkRAsEhvWWHUd9919o+LZ3N39qpX3AVd+PrqH88xYJASLhGCR+Hnnk6ai38Gu+5m57uiY0Zgr67Z3MmORECwSgkXiwzVW0Z+pXovs2qO6e62rlT2nldr8Gd4r5AsIFgnBIhHWYxX7OkUPguK9wl21U9cx636ke2vqzVgkBIuEYJEI97FW/t7v2tMaqeuuZo6/evMzzc+YsUgIFgnBInFojTVz7sy1RoqarfqZ48n6rZFuLWvGIiFYJASLxIfPCovv8a1ca8auc+/+/O7vW+yl3X3+uM6MRUKwSAgWiQ37WFcn36Er6sRPHl/0Zdi1xl1fb5mxSAgWCcEisbl3Q/0Nvjfvge3qR/XU+5V7mbFICBYJwSLx4bPCXb0JVmqh7tr1DZyn9s92nTsz5pV6LF5EsEgIFolD+1j1e4Iz9zBzPzNj7tpLu3s/xT7W3XuYZ8YiIVgkBIvEhn2smTrrkbd9N6Z+3rdy3V3fCJoZc3TMPDMWCcEiIVgkNrxXOFI875uxaz+mqP3a9cxxV08y7xXyZQSLhGCReKDm/erN/bFG97CrT32xhiv6aNjH4kUEi4RgkXhRPdbIU/0gijr30bVmxjlTR/XntayxeBHBIiFYJMI11lO9Q4u1yMmasLexxuJFBIuEYJHYvMaaumTcE2vm3Kf2nO4qxj/TK8uMRUKwSAgWic3fKyzOLdYrTz03rL/zWNTDfcaMRUKwSAgWiQ37WCs15ru+GzNz/IyTterF/YyOmbkf+1h8AcEiIVgkHvhe4d3ji3cPV8492eP07jGj40e8V8iXESwSgkUirMeq1w1F//TRuSf7o9a1+WeYsUgIFgnBIrG5z/vJ/ky7aqpW+nbO2NWffWUNd7Lvw7/MWCQEi4Rgkdj8rHDFyX2soua96NE149leoyNmLBKCRUKwSBx6r7Co3yq+1XOynmxXXdc769/NWCQEi4Rgkdi8xjrZJ31FUSu2cq1izGf/zc1YJASLhGCR+LAea0bR83NkV336yv3U30Ncudau2q95ZiwSgkVCsEhsXmPV/UiL9cfdNcro3BnF3tjdn+uPxRcTLBKCReKBb+nMqL97s3JMrdj3Ol8Lb8YiIVgkBIvEht4Nu1z/lp9cZ8zcT91rvu4fNrqWfSy+jGCRECwSD3yvcGack/XaJ/e0iueD9Xud9rF4EcEiIVgkNtdjveH9wZPvDO7qfToyM85TzzT/mxmLhGCRECwS4XuFu5x8J+7untbourt63+96D2Bk5v4/Y8YiIVgkBIvEF6yxrlZ6E8yMszJmPc5I0XN1nRmLhGCRECwSD/dumBmn6FHe7d98dj8r4+yq99cfiy8gWCQEi8SGNVa9N3P3Od2u617N1F3V31J8ql/8Z8xYJASLhGCReGl/LL6dGYuEYJEQLBKCRUKwSAgWCcEiIVgkBIuEYJEQLBKCRUKwSAgWCcEiIVgkBIvEPwF5xm/sFKlrAAAAAElFTkSuQmCC\",\n        \"secretKey\": \"GY4GIN3FGM3TQZTFGFRWKZLGMMZWMNRR\",\n        \"backupCodes\": [\n            \"8LgtxIgOvt\",\n            \"LrLmeryfCq\",\n            \"ytqt1RBbEM\",\n            \"CEF4rsN4yI\",\n            \"fBU0K3Zrwe\",\n            \"KAgvEydHGW\",\n            \"4ajP5KvOWM\",\n            \"IZS8WXC7CO\",\n            \"mBJ8QUK1gU\",\n            \"E4nUSomNqx\"\n        ]\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/googleauthenticator.coffee",
    "groupTitle": "GoogleAuthenticator"
  },
  {
    "type": "post",
    "url": "/GoogleAuthenticator/regenerateBackupCodes",
    "title": "Regenrate and get backup codes",
    "name": "PostGoogleAuthenticatorRegenerateBackupCodes",
    "group": "GoogleAuthenticator",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Code generated by Google Authenticator to regenerate backup codes.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "data.backupCodes",
            "description": "<p>Backup codes.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"backupCodes\": [\n            \"8LgtxIgOvt\",\n            \"LrLmeryfCq\",\n            \"ytqt1RBbEM\",\n            \"CEF4rsN4yI\",\n            \"fBU0K3Zrwe\",\n            \"KAgvEydHGW\",\n            \"4ajP5KvOWM\",\n            \"IZS8WXC7CO\",\n            \"mBJ8QUK1gU\",\n            \"E4nUSomNqx\"\n        ]\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 403 - badCode": [
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badCode&quot;.</p>"
          }
        ],
        "Error 401 - notDoubleAuthGA": [
          {
            "group": "Error 401 - notDoubleAuthGA",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Error 401 - notDoubleAuthGA",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;notDoubleAuthGA&quot;.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/googleauthenticator.coffee",
    "groupTitle": "GoogleAuthenticator"
  },
  {
    "type": "get",
    "url": "/home/languages",
    "title": "Get available languages",
    "name": "GetHomeLanguages",
    "group": "Home",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>An object containing languages (key: code, value: language name)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"en\": \"English\",\n        \"fr\": \"Français\"\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/home.coffee",
    "groupTitle": "Home"
  },
  {
    "type": "post",
    "url": "/invitation",
    "title": "Add a new invitation",
    "name": "PostInvitation",
    "group": "Invitation",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>Mail of invited person</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 201",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 201",
            "type": "Int",
            "optional": false,
            "field": "data.invitation_id",
            "description": "<p>Invitation ID.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 201,\n    \"status\": \"success\",\n    \"data\": {\n        \"invitation_id\": 1\n    },\n    \"message\": \"created\",\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 - mailFormat": [
          {
            "group": "Error 400 - mailFormat",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Mail format is incorrect.</p>"
          }
        ],
        "Error 400 - emptyField": [
          {
            "group": "Error 400 - emptyField",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Some parameters are missing.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/invitation.coffee",
    "groupTitle": "Invitation"
  },
  {
    "type": "post",
    "url": "/invitation/valid",
    "title": "Check if an invitation is valid",
    "name": "ValidInvitation",
    "group": "Invitation",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "iid",
            "description": "<p>Invitation ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Secret key</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {},\n    \"message\": null,\n    \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Either not valid, or already logged.</p>"
          }
        ],
        "Error 400 - emptyField": [
          {
            "group": "Error 400 - emptyField",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Some parameters are missing.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/invitation.coffee",
    "groupTitle": "Invitation"
  },
  {
    "type": "get",
    "url": "/lostpass/key/:uid/:key",
    "title": "Verifying with uid/key in URL",
    "name": "GetLostpassKey",
    "group": "Lostpass",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Lostpass key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;ok&quot;. The key is correct.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - differentKey": [
          {
            "group": "Error 400 - differentKey",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;differentKey&quot;. Lostpass key is different than key sent.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/lostpass.coffee",
    "groupTitle": "Lostpass"
  },
  {
    "type": "post",
    "url": "/lostpass",
    "title": "Change password",
    "name": "PostLostpass",
    "group": "Lostpass",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Lostpass key.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password already hashed with mui_hash.js. You have to check length and if password confirm match in client side.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - updated": [
          {
            "group": "Success 200 - updated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;updated&quot;. Password was updated successfully.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - differentKey": [
          {
            "group": "Error 400 - differentKey",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;differentKey&quot;. Lostpass key is different than key sent.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/lostpass.coffee",
    "groupTitle": "Lostpass"
  },
  {
    "type": "post",
    "url": "/lostpass/key",
    "title": "Verifying with uid/key",
    "name": "PostLostpassKey",
    "group": "Lostpass",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Lostpass key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;ok&quot;. The key is correct.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - differentKey": [
          {
            "group": "Error 400 - differentKey",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;differentKey&quot;. Lostpass key is different than key sent.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/lostpass.coffee",
    "groupTitle": "Lostpass"
  },
  {
    "type": "post",
    "url": "/lostpass/mail",
    "title": "Send a lost pass mail",
    "name": "PostLostpassMail",
    "group": "Lostpass",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": true,
            "field": "uid",
            "description": "<p>Agent id, if not specified, you must send login.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "login",
            "description": "<p>Login or email, if not specified, you must send uid.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - sent": [
          {
            "group": "Success 200 - sent",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;sent&quot;. Mail sent successfully.</p>"
          },
          {
            "group": "Success 200 - sent",
            "type": "Int",
            "optional": false,
            "field": "data",
            "description": "<p>UID.</p>"
          }
        ],
        "Success 200 - wait": [
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;wait&quot;. Mail was not sent because a mail has been already sent recently.</p>"
          },
          {
            "group": "Success 200 - wait",
            "type": "Int",
            "optional": false,
            "field": "data",
            "description": "<p>UID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - unknownAgent": [
          {
            "group": "Error 400 - unknownAgent",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;unknownAgent&quot;. This agent does not exist.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/lostpass.coffee",
    "groupTitle": "Lostpass"
  },
  {
    "type": "post",
    "url": "/request",
    "title": "New request",
    "name": "AddRequest",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Request title.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Content.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "author_email",
            "description": "<p>Author mail.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lang",
            "description": "<p>Author lang.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Author account type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "operating_system",
            "description": "<p>Operating system.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "web_browser",
            "description": "<p>Web browser.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "web_browser_version",
            "description": "<p>Web browser version.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.request_id",
            "description": "<p>Request ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.secret_key",
            "description": "<p>Secret key.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"request_id\": 1,\n        \"secret_key\": \"oAPst5MPDnc2gNxnqJuCRJRC4T51P73VAZzVNFkyyx8=\"\n    },\n    \"message\": null,\n    \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 - emptyField": [
          {
            "group": "Error 400 - emptyField",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Some parameters are missing.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "post",
    "url": "/request/closeRequest",
    "title": "Close an existing request",
    "name": "CloseRequest",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "secret_key",
            "description": "<p>Secret Key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;closed&quot;. Request is now closed.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;notAuthorized&quot;. Either not logged (agent side) or bad secret key.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "get",
    "url": "/request/closed",
    "title": "Get closed requests",
    "name": "ClosedRequest",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.closed",
            "description": "<p>Is closed ?</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"status\": \"success\",\n  \"data\": [\n    {\n      \"id\": \"1\",\n      \"author_email\": \"mail@mail.com\",\n      \"title\": \"Title\",\n      \"operating_system\": \"Windows 10\",\n      \"web_browser\": \"Chrome\",\n      \"web_browser_version\": \"69.0.3497.100\",\n      \"type\": \"Type.defaultUserType\",\n      \"creation_date\": \"2018-10-02 14:54:27\",\n      \"last_answer_date\": \"2018-10-02 14:54:27\",\n      \"lang\": \"fr\"\n    },\n    {\n      \"id\": \"2\",\n      \"author_email\": \"mail2@pm.me\",\n      \"title\": \"Title test\",\n      \"operating_system\": \"Linux x86_64\",\n      \"web_browser\": \"Firefox\",\n      \"web_browser_version\": \"60.0\",\n      \"type\": \"Type.defaultUserType\",\n      \"creation_date\": \"2018-10-02 15:03:50\",\n      \"last_answer_date\": \"2018-10-02 15:05:58\",\n      \"lang\": \"en\"\n    }\n  ],\n  \"message\": null,\n  \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Not logged in.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "post",
    "url": "/request/count",
    "title": "Get number of opened request",
    "name": "CountRequest",
    "group": "Request",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.count",
            "description": "<p>Count of opened request</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"count\": 15\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Not logged</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "post",
    "url": "/request/getAnswers",
    "title": "Get answers",
    "name": "GetAnswers",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "secret_key",
            "description": "<p>Secret Key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Contains all answers_id.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;notAuthorized&quot;. Either not logged (agent side) or bad secret key. { &quot;code&quot;: 200, &quot;status&quot;: &quot;success&quot;, &quot;data&quot;: [ { &quot;id&quot;: 1, &quot;id_request&quot;: 1, &quot;content&quot;: &quot;I got an error here : x&quot;, &quot;date&quot;: &quot;2018-09-29 23:37:08&quot;, &quot;name&quot;: &quot;Default.userName&quot;, &quot;is_agent&quot;: false } ], &quot;message&quot;: null, &quot;token&quot;: &quot;eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM&quot; }</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "get",
    "url": "/request",
    "title": "Get opened requests",
    "name": "GetRequest",
    "group": "Request",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "data.id",
            "description": "<p>Request ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.author_email",
            "description": "<p>Author mail</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.operating_system",
            "description": "<p>Operating system</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.web_browser",
            "description": "<p>Web browser</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.web_browser_version",
            "description": "<p>Web browser version</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.last_answer_date",
            "description": "<p>Last answer date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.lang",
            "description": "<p>Language.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"code\": 200,\n  \"status\": \"success\",\n  \"data\": [\n    {\n      \"id\": 1,\n      \"author_email\": \"mail@mail.com\",\n      \"title\": \"Title\",\n      \"operating_system\": \"Windows 10\",\n      \"web_browser\": \"Chrome\",\n      \"web_browser_version\": \"69.0.3497.100\",\n      \"type\": \"Type.defaultUserType\",\n      \"creation_date\": \"2018-10-02 14:54:27\",\n      \"last_answer_date\": \"2018-10-02 14:54:27\",\n      \"lang\": \"fr\"\n    },\n    {\n      \"id\": 2,\n      \"author_email\": \"mail2@pm.me\",\n      \"title\": \"Title test\",\n      \"operating_system\": \"Linux x86_64\",\n      \"web_browser\": \"Firefox\",\n      \"web_browser_version\": \"60.0\",\n      \"type\": \"Type.defaultUserType\",\n      \"creation_date\": \"2018-10-02 15:03:50\",\n      \"last_answer_date\": \"2018-10-02 15:05:58\",\n      \"lang\": \"en\"\n    }\n  ],\n  \"message\": null,\n  \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Not logged in.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "get",
    "url": "/request/infos",
    "title": "Get others infos of a request",
    "name": "Infos2Request",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.title",
            "description": "<p>Request title</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.closed",
            "description": "<p>Is it closed ?</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.operating_system",
            "description": "<p>Operating system</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.web_browser",
            "description": "<p>Web browser</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.web_browser_version",
            "description": "<p>Web browser version</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n      \"title\": \"Title test\",\n      \"closed\": false,\n      \"operating_system\": \"Windows 10\",\n      \"web_browser\": \"Chrome\",\n      \"web_browser_version\": \"69.854.10.24\"\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 - emptyField": [
          {
            "group": "Error 400 - emptyField",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Some parameters are missing.</p>"
          }
        ],
        "Error 400 - badID": [
          {
            "group": "Error 400 - badID",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bad ID format.</p>"
          }
        ],
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Either not logged in or invalid secret key</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "get",
    "url": "/request",
    "title": "Get infos of a request",
    "name": "InfosRequest",
    "group": "Request",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "request_id",
            "description": "<p>Request ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.request_id",
            "description": "<p>Request ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.secret_key",
            "description": "<p>Secret key.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": {\n        \"request_id\": 1,\n        \"secret_key\": \"oAPst5MPDnc2gNxnqJuCRJRC4T51P13VAZzVNFkyyx8=\"\n    },\n    \"message\": null,\n    \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 - emptyField": [
          {
            "group": "Error 400 - emptyField",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Some parameters are missing.</p>"
          }
        ],
        "Error 400 - badID": [
          {
            "group": "Error 400 - badID",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bad ID format.</p>"
          }
        ],
        "Error 401 - notAuthorized": [
          {
            "group": "Error 401 - notAuthorized",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Either not logged in or invalid secret key</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/request.coffee",
    "groupTitle": "Request"
  },
  {
    "type": "delete",
    "url": "/session",
    "title": "Delete the session",
    "name": "DeleteSession",
    "group": "Session",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;removeToken&quot;. Token has to be removed from client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "delete",
    "url": "/session/all",
    "title": "Delete all sessions",
    "name": "DeleteSessionAll",
    "group": "Session",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;removeToken&quot;. Token has to be removed from client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "delete",
    "url": "/session/jti/:jti",
    "title": "Delete the session with jti",
    "description": "<p>Jti is a base64 string, in order to pass it in the url, base64url standard must be respected (- instead + and _ instead /)</p>",
    "name": "DeleteSessionJti",
    "group": "Session",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": true,
            "field": "message",
            "description": "<p>&quot;removeToken&quot;. Token has to be removed from client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "get",
    "url": "/session",
    "title": "Get UID and list of sessions from token",
    "name": "GetSession",
    "group": "Session",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token (Authorization: Bearer).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Current token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "data.uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.tokens",
            "description": "<p>List of sessions.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.tokens.jti",
            "description": "<p>Token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "data.tokens.iat",
            "description": "<p>Issued at.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.tokens.current",
            "description": "<p>Current session?</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/session",
    "title": "API Authentication",
    "name": "PostSession",
    "group": "Session",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>Login (or email).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password already hashed with mui_hash.js.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - Ok": [
          {
            "group": "Success 200 - Ok",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Generated token</p>"
          },
          {
            "group": "Success 200 - Ok",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200 - Ok",
            "type": "Int",
            "optional": false,
            "field": "data.uid",
            "description": "<p>UID.</p>"
          },
          {
            "group": "Success 200 - Ok",
            "type": "String",
            "optional": false,
            "field": "data.lang",
            "description": "<p>Agent language.</p>"
          }
        ],
        "Success 200 - doubleAuth": [
          {
            "group": "Success 200 - doubleAuth",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;doubleAuth&quot;. 2-factor authentication is enabled, credentials are valid but you need to send them again with code sent by mail.</p>"
          },
          {
            "group": "Success 200 - doubleAuth",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200 - doubleAuth",
            "type": "Int",
            "optional": false,
            "field": "data.uid",
            "description": "<p>UID.</p>"
          },
          {
            "group": "Success 200 - doubleAuth",
            "type": "String",
            "optional": false,
            "field": "data.lang",
            "description": "<p>Agent language.</p>"
          },
          {
            "group": "Success 200 - doubleAuth",
            "type": "Int",
            "optional": false,
            "field": "data.doubleAuthMethod",
            "description": "<p>1: by mail, 2: Google Auth.</p>"
          }
        ],
        "Success 200 - wait": [
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;wait&quot;. 2-factor authentication is enabled, credentials are valid but you need to send them again with code sent by mail. Mail was not sent because a mail has been already sent recently.</p>"
          },
          {
            "group": "Success 200 - wait",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200 - wait",
            "type": "Int",
            "optional": false,
            "field": "data.uid",
            "description": "<p>UID.</p>"
          },
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "data.lang",
            "description": "<p>Agent language.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 401 - validate": [
          {
            "group": "Error 401 - validate",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;validate&quot;. Account is not validated but credentials are valid.</p>"
          },
          {
            "group": "Error 401 - validate",
            "type": "Int",
            "optional": false,
            "field": "data",
            "description": "<p>UID.</p>"
          }
        ],
        "Error 401 - badAgent": [
          {
            "group": "Error 401 - badAgent",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badAgent&quot;. Agent does not exist.</p>"
          }
        ],
        "Error 401 - badPass": [
          {
            "group": "Error 401 - badPass",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badPass&quot;. Agent exists but incorrect password.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "post",
    "url": "/session/authcode",
    "title": "2-factor API Authentication",
    "name": "PostSessionAuthCode",
    "group": "Session",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password hashed.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Code sent by mail or Google Authenticator.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - Ok": [
          {
            "group": "Success 200 - Ok",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Generated token</p>"
          },
          {
            "group": "Success 200 - Ok",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200 - Ok",
            "type": "Int",
            "optional": false,
            "field": "data.uid",
            "description": "<p>UID.</p>"
          },
          {
            "group": "Success 200 - Ok",
            "type": "Int",
            "optional": false,
            "field": "data.lang",
            "description": "<p>Agent language.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 401 - validate": [
          {
            "group": "Error 401 - validate",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;validate&quot;. Account is not validated but credentials are valid.</p>"
          },
          {
            "group": "Error 401 - validate",
            "type": "Int",
            "optional": false,
            "field": "data",
            "description": "<p>UID.</p>"
          }
        ],
        "Error 403 - badCode": [
          {
            "group": "Error 403 - badCode",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badCode&quot;. Agent exists but incorrect code.</p>"
          }
        ],
        "Error 403 - badPass": [
          {
            "group": "Error 403 - badPass",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;badPass&quot;. Agent exists but incorrect password.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/session.coffee",
    "groupTitle": "Session"
  },
  {
    "type": "get",
    "url": "/validate/key/:uid/:key",
    "title": "Validation with uid/key in URL",
    "name": "GetValidateKey",
    "group": "Validate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Validation key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - validated": [
          {
            "group": "Success 200 - validated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;validated&quot;. Account is now validated.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - differentKey": [
          {
            "group": "Error 400 - differentKey",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;differentKey&quot;. Validation key is different than key sent.</p>"
          }
        ],
        "Error 400 - alreadyValidated": [
          {
            "group": "Error 400 - alreadyValidated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;alreadyValidated&quot;. This account is already validated.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/validate.coffee",
    "groupTitle": "Validate"
  },
  {
    "type": "post",
    "url": "/validate/key",
    "title": "Validation with uid/key",
    "name": "PostValidateKey",
    "group": "Validate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Validation key.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - validated": [
          {
            "group": "Success 200 - validated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;validated&quot;. Account is now validated.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - differentKey": [
          {
            "group": "Error 400 - differentKey",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;differentKey&quot;. Validation key is different than key sent.</p>"
          }
        ],
        "Error 400 - alreadyValidated": [
          {
            "group": "Error 400 - alreadyValidated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;alreadyValidated&quot;. This account is already validated.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/validate.coffee",
    "groupTitle": "Validate"
  },
  {
    "type": "post",
    "url": "/validate/mail",
    "title": "Send a validation mail",
    "name": "PostValidateMail",
    "group": "Validate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "uid",
            "description": "<p>Agent id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200 - sent": [
          {
            "group": "Success 200 - sent",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;sent&quot;. Mail sent successfully.</p>"
          }
        ],
        "Success 200 - wait": [
          {
            "group": "Success 200 - wait",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;wait&quot;. Mail was not sent because a mail has been already sent recently.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 - alreadyValidated": [
          {
            "group": "Error 400 - alreadyValidated",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>&quot;alreadyValidated&quot;. This account is already validated.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/validate.coffee",
    "groupTitle": "Validate"
  },
  {
    "type": "get",
    "url": "/version",
    "title": "Get server version",
    "name": "GetVersion",
    "group": "Version",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Current version.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"code\": 200,\n    \"status\": \"success\",\n    \"data\": \"v0.0.1\",\n    \"message\": null,\n    \"token\": null\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/version.coffee",
    "groupTitle": "Version"
  }
] });
