###
@api 		{post} 		/answer 				Add an answer
@apiName 	AddAnswer
@apiGroup 	Answer

@apiParam 									{Int} request_id 		    Request id.
@apiParam 									{String} content 		    Content.
@apiParam 									{String} [secret_key]   Secret Key.

@apiSuccess (Success 200) 		{Object} data	Contains answer_id.

@apiError 	(Error 401 - notAuthorized)		{String} message	"notAuthorized". Not logged (agent side) or bad secret key.
@apiError 	(Error 401 - requestClosed)		{String} message	"requestClosed". Request is closed.
@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {
        "answer_id": 1,
        "email": "mikayla@mikayla.ee",
        "registration_date": 1517233312,
        "double_auth": 0
    },
    "message": "sent",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}
###
