###
@api 		{post} 		/request 				New request
@apiName 	  AddRequest
@apiGroup 	Request

@apiParam 									{String} title 		              Request title.
@apiParam 									{String} content 		            Content.
@apiParam 									{String} author_email 		      Author mail.
@apiParam 									{String} lang 		              Author lang.
@apiParam 									{String} type 		              Author account type.
@apiParam 									{String} lang 		              Author lang.
@apiParam 									{String} operating_system 		  Operating system.
@apiParam 									{String} web_browser 		        Web browser.
@apiParam 									{String} web_browser_version 		Web browser version.

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {String}    data.request_id  Request ID
@apiSuccess (Success 200) 	  {String}    data.secret_key  Secret key.

@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {
        "request_id": 1,
        "secret_key": "oAPst5MPDnc2gNxnqJuCRJRC4T51P73VAZzVNFkyyx8="
    },
    "message": null,
    "token": null
}

@apiError 	(Error 400 - emptyField) 	{String} 	message					Some parameters are missing.
###

###
@api 		{get} 		/request 				Get opened requests
@apiName 	  GetRequest
@apiGroup 	Request

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {Int}       data.id                   Request ID
@apiSuccess (Success 200) 	  {String}    data.author_email         Author mail
@apiSuccess (Success 200) 	  {String}    data.title                Title
@apiSuccess (Success 200) 	  {String}    data.operating_system     Operating system
@apiSuccess (Success 200) 	  {String}    data.web_browser          Web browser
@apiSuccess (Success 200) 	  {String}    data.web_browser_version  Web browser version
@apiSuccess (Success 200) 	  {String}    data.last_answer_date     Last answer date.
@apiSuccess (Success 200) 	  {String}    data.lang                 Language.

@apiSuccessExample {json} Success-Response:
{
  "code": 200,
  "status": "success",
  "data": [
    {
      "id": 1,
      "author_email": "mail@mail.com",
      "title": "Title",
      "operating_system": "Windows 10",
      "web_browser": "Chrome",
      "web_browser_version": "69.0.3497.100",
      "type": "Type.defaultUserType",
      "creation_date": "2018-10-02 14:54:27",
      "last_answer_date": "2018-10-02 14:54:27",
      "lang": "fr"
    },
    {
      "id": 2,
      "author_email": "mail2@pm.me",
      "title": "Title test",
      "operating_system": "Linux x86_64",
      "web_browser": "Firefox",
      "web_browser_version": "60.0",
      "type": "Type.defaultUserType",
      "creation_date": "2018-10-02 15:03:50",
      "last_answer_date": "2018-10-02 15:05:58",
      "lang": "en"
    }
  ],
  "message": null,
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 401 - notAuthorized) 	{String} 	message					Not logged in.
###

###
@api 		{post} 		/request/closeRequest 				Close an existing request
@apiName 	CloseRequest
@apiGroup 	Request

@apiParam 									{String} request_id 		  Request ID.
@apiParam 									{String} [secret_key] 		  Secret Key.

@apiSuccess (Success 200) {String} message	"closed". Request is now closed.

@apiError 	(Error 401 - notAuthorized)	{String} message	"notAuthorized". Either not logged (agent side) or bad secret key.
###

###
@api 		{post} 		/request/getAnswers 				Get answers
@apiName 	GetAnswers
@apiGroup 	Request

@apiParam   {String}    request_id 		  Request ID.
@apiParam   {String}    [secret_key] 	  Secret Key.

@apiSuccess (Success 200) 		{Object} data	Contains all answers_id.

@apiError 	(Error 401 - notAuthorized)	{String} message	"notAuthorized". Either not logged (agent side) or bad secret key.
{
  "code": 200,
  "status": "success",
  "data": [
    {
      "id": 1,
      "id_request": 1,
      "content": "I got an error here : x",
      "date": "2018-09-29 23:37:08",
      "name": "Default.userName",
      "is_agent": false
    }
  ],
  "message": null,
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

###

###
@api 		{get} 		/request 				Get infos of a request
@apiName 	  InfosRequest
@apiGroup 	Request

@apiParam 	{int}   request_id    Request ID.

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {String}    data.request_id  Request ID
@apiSuccess (Success 200) 	  {String}    data.secret_key  Secret key.

@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {
        "request_id": 1,
        "secret_key": "oAPst5MPDnc2gNxnqJuCRJRC4T51P13VAZzVNFkyyx8="
    },
    "message": null,
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 400 - emptyField) 	{String} 	message					Some parameters are missing.
@apiError 	(Error 400 - badID) 	{String} 	message					    Bad ID format.
@apiError 	(Error 401 - notAuthorized) 	{String} 	message			Either not logged in or invalid secret key
###

###
@api 		{get} 		/request/infos 				Get others infos of a request
@apiName 	  Infos2Request
@apiGroup 	Request

@apiParam 	{int}   request_id    Request ID.

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {String}    data.title                Request title
@apiSuccess (Success 200) 	  {Boolean}   data.closed               Is it closed ?
@apiSuccess (Success 200) 	  {String}    data.operating_system     Operating system
@apiSuccess (Success 200) 	  {String}    data.web_browser          Web browser
@apiSuccess (Success 200) 	  {String}    data.web_browser_version  Web browser version

@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {
      "title": "Title test",
      "closed": false,
      "operating_system": "Windows 10",
      "web_browser": "Chrome",
      "web_browser_version": "69.854.10.24"
    },
    "message": null,
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 400 - emptyField) 	    {String}  message			Some parameters are missing.
@apiError 	(Error 400 - badID) 	        {String} 	message			Bad ID format.
@apiError 	(Error 401 - notAuthorized) 	{String} 	message			Either not logged in or invalid secret key
###

###
@api 		{post} 		/request/count 				Get number of opened request
@apiName 	  CountRequest
@apiGroup 	Request

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {String}    data.count  Count of opened request

@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {
        "count": 15
    },
    "message": null,
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 401 - notAuthorized) 	{String} 	message   Not logged
###

###
@api 		{get} 		/request/closed 				Get closed requests
@apiName 	  ClosedRequest
@apiGroup 	Request

@apiParam 	{int}   request_id    Request ID.

@apiSuccess (Success 200)     {Object}    data
@apiSuccess (Success 200) 	  {Boolean}   data.closed  Is closed ?

@apiSuccessExample {json} Success-Response:
{
  "code": 200,
  "status": "success",
  "data": [
    {
      "id": "1",
      "author_email": "mail@mail.com",
      "title": "Title",
      "operating_system": "Windows 10",
      "web_browser": "Chrome",
      "web_browser_version": "69.0.3497.100",
      "type": "Type.defaultUserType",
      "creation_date": "2018-10-02 14:54:27",
      "last_answer_date": "2018-10-02 14:54:27",
      "lang": "fr"
    },
    {
      "id": "2",
      "author_email": "mail2@pm.me",
      "title": "Title test",
      "operating_system": "Linux x86_64",
      "web_browser": "Firefox",
      "web_browser_version": "60.0",
      "type": "Type.defaultUserType",
      "creation_date": "2018-10-02 15:03:50",
      "last_answer_date": "2018-10-02 15:05:58",
      "lang": "en"
    }
  ],
  "message": null,
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 401 - notAuthorized) 	{String} 	message			Not logged in.
###