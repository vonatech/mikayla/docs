###
@api 		{get} 		/version		Get server version
@apiName 	GetVersion
@apiGroup 	Version

@apiSuccess (Success 200) 		{String} data		Current version.

@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": "v0.0.1",
    "message": null,
    "token": null
}
###
