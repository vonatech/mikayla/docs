###
@api 		  {post} 		/invitation		Add a new invitation
@apiName 	PostInvitation
@apiGroup Invitation

@apiHeader  {String} 	    token 		 Token (Authorization: Bearer).

@apiParam   {String}    mail 		Mail of invited person

@apiSuccess (Success 201)     {String} 	  token               Current token.
@apiSuccess (Success 201)     {Object}    data
@apiSuccess (Success 201) 	  {Int}       data.invitation_id  Invitation ID.
@apiSuccessExample {json} Success-Response:
{
    "code": 201,
    "status": "success",
    "data": {
        "invitation_id": 1
    },
    "message": "created",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1MTk4OTI3MTMsImV4cCI6MTU1MTQyODcxMywiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9._PWCLR7lgLmX3qA3gVnAhyGj-wt3WjDeNaml_tOunJM"
}

@apiError 	(Error 400 - mailFormat) 	{String} 	message					Mail format is incorrect.
@apiError 	(Error 400 - emptyField) 	{String} 	message					Some parameters are missing.
###

###
@api 		  {post} 		/invitation/valid		Check if an invitation is valid
@apiName 	ValidInvitation
@apiGroup Invitation

@apiHeader  {String} 	    token 		 Token (Authorization: Bearer).

@apiParam   {Int}       iid 		Invitation ID
@apiParam   {String}    key 		Secret key

@apiSuccess (Success 200)     {Object}    data
@apiSuccessExample {json} Success-Response:
{
    "code": 200,
    "status": "success",
    "data": {},
    "message": null,
    "token": null
}

@apiError 	(Error 401 - notAuthorized) 	{String} 	message     Either not valid, or already logged.
@apiError 	(Error 400 - emptyField) 	    {String} 	message			Some parameters are missing.
###